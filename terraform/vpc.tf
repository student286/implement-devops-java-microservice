module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  name            = var.vpc_name
  cidr            = var.vpc_cidr
  azs             = [var.zone_1, var.zone_3]
  private_subnets = [var.private_subnet_cidr_1, var.private_subnet_cidr_3]
  public_subnets  = [var.public_subnet_cidr_1, var.public_subnet_cidr_3]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform   = "true"
    Environment = "Prod"
  }

  vpc_tags = {
    Name = var.vpc_name
  }
}

