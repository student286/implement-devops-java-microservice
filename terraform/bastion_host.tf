resource "aws_instance" "vprofile_bastion" {
  ami                         = lookup(var.amis, var.region)
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.vprofile_key.key_name
  subnet_id                   = module.vpc.public_subnets[0]
  count                       = var.instance_count
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.vprofile_bastion_sg.id]

  tags = {
    Name    = "Vprofile-Bastion"
    Project = "Vprofile"

  }

  provisioner "file" {
    content = templatefile("db-deploy.tmpl", { rds-endpoint = aws_db_instance.vprofile_rds.address, dbuser = var.db_user, dbpass = var.db_pass })

    destination = "/tmp/vprofile-db-deploy.sh"

  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/vprofile-db-deploy.sh",
      "sudo /tmp/vprofile-db-deploy.sh"
    ]
  }

  connection {
    user        = var.username
    private_key = file(var.private_key_path)
    host        = self.public_ip
  }

  depends_on = [aws_db_instance.vprofile_rds]

}

