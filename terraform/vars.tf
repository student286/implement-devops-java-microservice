variable "region" {
  default = "us-west-2"
}

variable "amis" {
  type = map(any)
  default = {
    us-east-1 = "ami-0fc5d935ebf8bc3bc"
    us-west-2 = "ami-0efcece6bed30fd98"
  }
}

variable "private_key_path" {
  default = "vprofilekey"
}

variable "public_key_path" {
  default = "vprofilekey.pub"
}

variable "username" {
  default = "ubuntu"
}

variable "my_ip" {
  default = "0.0.0.0/0"
}

variable "rmq_user" {
  default = "rabbit"
}

variable "rmq_pass" {
  default = "password123456"
}

variable "db_user" {
  default = "admin"
}

variable "db_pass" {
  default = "admin123"
}

variable "db_name" {
  default = "accounts"
}

variable "instance_count" {
  default = "1"
}

variable "vpc_name" {
  default = "vprofile-vpc"
}

variable "zone_1" {
  default = "us-west-2a"
}

variable "zone_3" {
  default = "us-west-2c"
}

variable "vpc_cidr" {
  default = "172.21.0.0/16"
}

variable "public_subnet_cidr_1" {
  default = "172.21.1.0/24"
}

variable "public_subnet_cidr_3" {
  default = "172.21.2.0/24"
}

variable "private_subnet_cidr_1" {
  default = "172.21.3.0/24"
}

variable "private_subnet_cidr_3" {
  default = "172.21.4.0/24"
}



