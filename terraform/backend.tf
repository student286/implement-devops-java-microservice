terraform {
  backend "s3" {
    bucket = "vprofile-tf-state-2"
    key    = "terraform/backend"
    region = "us-west-2"
  }
}

