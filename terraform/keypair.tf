resource "aws_key_pair" "vprofile_key" {
  key_name   = "vprofile-key"
  public_key = file(var.public_key_path)
}

