resource "aws_db_subnet_group" "vprofile_rds_subnet_grp" {
  name       = "vprofile-rds-subnet-group"
  subnet_ids = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
  tags = {
    Name = "Subnet group for RDS"
  }
}

resource "aws_elasticache_subnet_group" "vprofile_ecache_subnet_grp" {
  name       = "vprofile-elasticache-subnet-group"
  subnet_ids = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
  tags = {
    Name = "Subnet group for Elasticache"
  }
}

resource "aws_db_instance" "vprofile_rds" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7.37"
  instance_class         = "db.t2.micro"
  db_name                = var.db_name
  username               = var.db_user
  password               = var.db_pass
  parameter_group_name   = "default.mysql5.7"
  multi_az               = "false"
  publicly_accessible    = "false"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.vprofile_rds_subnet_grp.name
  vpc_security_group_ids = [aws_security_group.vprofile_backend_sg.id]

}

resource "aws_elasticache_cluster" "vprofile_elasticache" {
  cluster_id           = "vprofile-ecache"
  engine               = "memcached"
  node_type            = "cache.t2.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.memcached1.6"
  port                 = 11211
  security_group_ids   = [aws_security_group.vprofile_backend_sg.id]
  subnet_group_name    = aws_elasticache_subnet_group.vprofile_ecache_subnet_grp.name

}

resource "aws_mq_broker" "vprofile_rmq" {
  broker_name        = "vprofile-rmq"
  engine_type        = "ActiveMQ"
  engine_version     = "5.15.16"
  host_instance_type = "mq.t2.micro"
  security_groups    = [aws_security_group.vprofile_backend_sg.id]
  subnet_ids         = [module.vpc.private_subnets[0]]

  user {
    username = var.rmq_user
    password = var.rmq_pass

  }
}

